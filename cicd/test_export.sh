#!/usr/bin/env sh
set -ex



exported_secrets='{
    "COVERITY_PASSPHRASE": "uGyW!GRlOS+o51l/8nw",
    "COVERITY_SCAN_EMAIL": "analytics-platform-vdk@vmware.com",
    "COVERITY_SCAN_PROJECT_NAME": "versatile-data-kit"}'

export $( echo "$exported_secrets" | jq -r 'to_entries[] | "\(.key)=\"\(.value)\""')


echo "curl  --form token=$COVERITY_SCAN_TOKEN  --form email=$COVERITY_SCAN_EMAIL --form file=@cov-int.tar.gz --form description="Coverity scan for vdk"  https://scan.coverity.com/builds?project=$COVERITY_SCAN_PROJECT_NAME"


echo "$exported_secrets" | jq -r 'to_entries[] | .key + "=" + .value'
#  while read -r line; do
#   export $line
# done


# export COVERITY_PASSPHRASE="uGyW!GRlOS+o51l/8nw"

# export COVERITY_SCAN_EMAIL="analytics-platform-vdk@vmware.com"

# export COVERITY_SCAN_PROJECT_NAME="versatile-data-kit"


export

echo "Staring the script"
echo $COVERITY_PASSPHRASE

echo $COVERITY_SCAN_EMAIL

echo $COVERITY_SCAN_PROJECT_NAME
